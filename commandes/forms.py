from django import forms
from .models import *

class UtilisateurForm(forms.ModelForm):
    class Meta:
        model = Utilisateur
        fields = ['username', 'password', 'admin']

class CommandeForm(forms.ModelForm):
    class Meta:
        model = Commande
        fields =['objet', 'prix', 'image', 'utilisateur']
