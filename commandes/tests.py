from django.test import TestCase
from commandes.models import *

class UtilisateurTestCase(TestCase):
    def setUp(self):
        Utilisateur.objects.create(username="Pierre", password ="azerty", admin=False)
    
    def test_password(self):
        Pierre= Utilisateur.objects.get(username="Pierre")
        self.assertEqual(Pierre.verifier_password("azerty"),True)
        self.assertEqual(Pierre.verifier_password("pas_azerty"),False)


class CommandeTestCase(TestCase):
    def setUp(self):
        Utilisateur.objects.create(username="Pierre", password ="azerty", admin=False)
        pierre=Utilisateur.objects.get(username="Pierre")
        Commande.objects.create(objet="pain", prix=3.4,image="pas d'image car trop gros",utilisateur=pierre)

    def test_commande_utilisateur(self):
        pierre= Utilisateur.objects.get(username="Pierre")
        commande=Commande.objects.get(objet="pain")
        self.assertIs(commande.utilisateur.id,pierre.id)



